# -*- coding: utf-8 -*-
layer = qgis.utils.iface.activeLayer()
selection = layer.selectedFeatures()
features = layer.getFeatures()

if len(selection) > 0:
    print("Usando seleções da camada")
    layer = selection
else:
    print("Não há seleções na camada")

def extensao():
    e = layer.extent()
    extensao = [e.xMinimum(), e.yMinimum(), e.xMaximum(), e.yMaximum()]
    return extensao


print(extensao())



## Criando a camada
#vl =  QgsVectorLayer("Point", "pontos_gerados_2", "memory")
#pr = vl.dataProvider()
#
#
## Add Fields
#pr.addAttributes([QgsField("linha", QVariant.String),
#                    QgsField("ponto", QVariant.String),
#                    QgsField("x", QVariant.Double),
#                    QgsField("y", QVariant.Double)])
#vl.updateFields()
#
## Add a Feature
#fet = QgsFeature()
#fet.setGeometry(QgsGeometry.fromPointXY(QgsPointXY(10, 10)))
#fet.setAttributes(["L_1", "P_1", 10, 10])
#pr.addFeatures([fet])
#
## Atualizando a extensão da camada
#vl.updateExtents()
