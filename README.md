# quinconcio_plugin

Plugin do qgis que calcula o plantio no formato quinconcio

lado = tamanho do lado utilizado em metros

x_min, y_min, x_max, y_max = Valores minimos e máximos de cada eixo (em utm)
area = area da geometria, em m2


## Linha de comando (Headless)
usage: python3 quin_headless.py \[-h\] --aresta ARESTA --x_min X_MIN --y_min Y_MIN
                         --x_max X_MAX --y_max Y_MAX \[--file OUTPUT\]

Versão linha de comando.

optional arguments:
  -h, --help            show this help message and exit
  --aresta ARESTA, -a ARESTA
                        Tamanho do lado a ser usado.
  --x_min X_MIN, -x X_MIN
                        Menor valor de X.
  --y_min Y_MIN, -y Y_MIN
                        Menor valor de y.
  --x_max X_MAX, -X X_MAX
                        Maior valor de X.
  --y_max Y_MAX, -Y Y_MAX
                        Maior valor de y.
  --file OUTPUT, -f OUTPUT
                        arquivo de saida.

Valores expressos em número decimais deverão ter a separação decimais o ponto.

### Arquivo de configuração

Modo de uso: python3 quin_headless.py \@arquivo.txt

No arquivo de configuração, aceita apenas as abreveaturas dos comandos
\[-a, -y, -x, -Y, -X, -f\]
cada comando deverá estar em uma linha seguido dos valores.
Exemplo:

-a 2.7
-y 30
-Y 70
-x 40
-X 45
-f '/home/Documents/config.txt'
