u""".

/***************************************************************************

Name            : Quinconcio Plugin.
Description     : Faz o calculo de plantio usando a forma quinconcio
                : Versão de linha de comando
                : http://agronomiarustica.com/sistema-de-plantio-em-quinconcio/
Date            : Março, 2019
copyright       : (C) 2019 by Kyle Felipe
email           : kylefelipe@gmail.com

 ***************************************************************************/
/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
"""

import formula
import argparse

parser = argparse.ArgumentParser(prog=u"Quincôncio Plugin",
                                 description=u"Versão linha de comando.",
                                 fromfile_prefix_chars='@')

parser.add_argument('--aresta', '-a',
                    action='store',
                    type=float,
                    dest='aresta',
                    required=True,
                    help='Tamanho do lado a ser usado.')
parser.add_argument('--x_min', '-x',
                    action='store',
                    dest='x_min',
                    required=True,
                    type=float,
                    help='Menor valor de X.')
parser.add_argument('--y_min', '-y',
                    action='store',
                    required=True,
                    type=float,
                    dest='y_min',
                    help='Menor valor de y.')
parser.add_argument('--x_max', '-X',
                    action='store',
                    required=True,
                    type=float,
                    dest='x_max',
                    help='Maior valor de X.')
parser.add_argument('--y_max', '-Y',
                    action='store',
                    required=True,
                    type=float,
                    dest='y_max',
                    help='Maior valor de y.')
parser.add_argument('--file', '-f',
                    action='store',
                    required=False,
                    type=str,
                    dest='output',
                    default='pontos_gerados.txt',
                    help='Endereço completo do arquivo de saida.')

arguments = parser.parse_args()

print(arguments)
h = formula.altura(arguments.aresta)
qt_lin = formula.qt_linhas(y_max=arguments.y_max,
                           y_min=arguments.y_min,
                           lado=arguments.aresta)
qt_col = formula.qt_colunas(x_max=arguments.x_max,
                            x_min=arguments.x_min,
                            lado=arguments.aresta)
pts = formula.gera_pontos(x_min=arguments.x_min,
                          y_min=arguments.y_min,
                          colunas=qt_col, linhas=qt_lin,
                          altura=h, lado=arguments.aresta)
formula.gera_csv(pontos=pts, output=arguments.output)
